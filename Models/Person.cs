﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class Person
    {
        public int id { set; get; }
        public string name { set; get; }
        public string phone { set; get; }
        [ForeignKey("work")]
        public int workid { set; get; }
        public work work { set; get; }
    }
}
