﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models;

namespace WebApplication1
{
    public class DataBase : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
                   => optionsBuilder.UseNpgsql("Host=localhost;Database=my_db;Username=postgres;Password=123");

        public DbSet<Person> Persons { set; get; }
        public DbSet<work>  Works { set; get; }
    }
}
