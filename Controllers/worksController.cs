﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication1;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class worksController : ControllerBase
    {

        DataBase _context = new DataBase();

        // GET: api/works
        [HttpGet]
        public IEnumerable<work> GetWorks()
        {
            return _context.Works;
        }

        // GET: api/works/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Getwork([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var work = await _context.Works.FindAsync(id);

            if (work == null)
            {
                return NotFound();
            }

            return Ok(work);
        }

        // PUT: api/works/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Putwork([FromRoute] int id, [FromBody] work work)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != work.id)
            {
                return BadRequest();
            }

            _context.Entry(work).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!workExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/works
        [HttpPost]
        public async Task<IActionResult> Postwork([FromBody] work work)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Works.Add(work);
            await _context.SaveChangesAsync();

            return CreatedAtAction("Getwork", new { id = work.id }, work);
        }

        // DELETE: api/works/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Deletework([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var work = await _context.Works.FindAsync(id);
            if (work == null)
            {
                return NotFound();
            }

            _context.Works.Remove(work);
            await _context.SaveChangesAsync();

            return Ok(work);
        }

        private bool workExists(int id)
        {
            return _context.Works.Any(e => e.id == id);
        }
    }
}